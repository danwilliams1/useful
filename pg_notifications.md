# Using Notifications in PostgreSQL

1. Create a trigger for the update:

```
CREATE OR REPLACE FUNCTION add_task_notify()
RETURNS trigger AS
$BODY$

BEGIN
PERFORM pg_notify('new_id', NEW.ID::text);
RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION add_task_notify()
OWNER TO postgres;

CREATE TRIGGER add_task_event_trigger
AFTER INSERT
ON test_table
FOR EACH ROW
EXECUTE PROCEDURE add_task_notify();
```

2. Create the notifications:

```
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import select

conn = psycopg2.connect()
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

cur = conn.cursor()

cur.execute("LISTEN new_id;")
print("Waiting for notifications on channel 'new_id'")

while True:
    if select.select([conn], [], [], 10) == ([], [], []):
        print("More than 10 seconds passed...")
    else:
        conn.poll()
        while conn.notifies:
            notify = conn.notifies.pop(0)
            print(f"Got NOTIFY: {notify.channel} - {notify.payload}")
```
