# Exploratory Data Analysis

## 1. What are the types of structured data?

There are two basic types of structured data: numeric and categorical. Numeric data comes in two forms: continuous, such as wind speed or time duration, and discrete, such as the count of the occurence of an event.

Categorical data takes only a fixed set of values, such as a type of TV screen (plasma, LCD, LED, …) or a state name (Alabama, Alaska, …)

## 2. What is categorical data?

Data that can only take on a specific set of values.

Binary data is an important special case of categorical data that takes on only one of two values, such as 0/1, yes/no or true/false.

Another useful type of categorical data is ordinal data in which the categories are ordered; an example of this is a numerical rating (1, 2, 3, 4, or 5).

## 3. What is a dataframe?

The basic data structure for statistical and machine learning models, a 2-dimensional matrix with rows indicating records and columns indicating features (variables).

## 4. Mean and weighed mean

The mean is the sum of all values divided by the number of values.

The weighed mean is the sum of all values times a weight divided by the sum of the weights.

## 5. Why use a weighed mean?

1. Some values are intrinsically more variable than others, and highly variable observations are given a lower weight. For example, if we are taking the average from multiple sensors and one of the sensors is less accurate, then we might downweight the data from that sensor.

2. The data collected does not equally represent the different groups that we are interested in measuring. To correct that, we can give a higher weight to the values from the groups that are underrepresented.

## 6. Median

The value such that one-half of the data lies above and below. The 50th percentile.

If there is an even number of data values, the middle value is one that is not actually in the data set, but rather is the average of the two values that divide the sorted data into upper and lower halves.

## 7. What are outliers?

An outlier is any value that is very distant from the other values in a dataset.

Being an outlier in itself does not make a data value invalid or erroneous but using the median instead of mean helps since it is not influenced by outliers (extreme cases) that could skew the results.

## 8. Why use a trimmed mean?

A trimmed mean is widely used to avoid the influence of outliers. For example, trimming the bottom and top 10% (a common choice) of the data will provide protection against outliers.

## 9. Anomaly detection

In anomaly detection the points of interest are the outliers, and the greater mass of data serves primarily to define the “normal” against which anomalies are measured.

## 10. What are the two dimensions in summarizing a feature?

1. Location, the estimate of where most of the data is located (tendency).
2. Variability, whether the data values are tightly clustered or spread out.

## 11. What is variance?

The sum of squared deviations from the mean divided by N-1,where N is the number of data values.

## 12. What is standard deviation?

The square root of the variance.

## 13. What is the mean absolute deviation?

The mean of the absolute value of the deviations from the mean.

## 14. Why choose standard deviation over mean absolute deviation?

Mathematically, working with squared values is much more convenient than absolute values, especially for statistical models.

## 15. What is a percentile?

The value such that P percent of the values take on this value or less and (100-P) percent take on this value or more.

## 16. Explain the degrees of freedom

Degrees of freedom take into account the number of constraints in computing an estimate.

In the case of standard deviation, there are N-1 degrees of freedom since there is one constraint: the standard deviation depends on calculating the sample mean.

## 17. Are variance and standard deviation robust to outliers?

No. The variance and standard deviation are especially sensitive to outliers since they are based on the squared deviations.

## 18. What is a robust estimate of variability?

An robust estimate of variability is the median absolute deviation from the median.

The median of the absolute value of the deviations from the median.

## 19. IQR

A common measurement of variability is the difference between the 25th percentile and the 75th percentile, called the interquartile range (or IQR).

## 20. What is a boxplot?

A plot to help visualize the distribution of data.

The top and bottom of the box are the 75th and 25th percentiles, respectively.

The median is shown by the horizontal line in the box.

The dashed lines, referred to as whiskers, extend from the top and bottom to indicate the range for the bulk of the data.

## 21. What is a frequency table?

A frequency table of a variable divides up the variable range into equally spaced segments, and tells us how many values fall in each segment.

## 22. What is a histogram?

A plot of the frequency table with the bins on the x-axis and the count (or proportion) on the y-axis.

## 23. What is a density plot?

A smoothed version of the histogram, often based on a density estimate.

## 24. What is a bar chart?

Used for categorical data, shows the frequency or proportion for each category plotted as bars.

## 25. Correlation

Measures the extent to which numeric variables are associated with one another (ranges from -1 to +1).

## 26. What is a correlation matrix?

A table where the variables are shown on both rows and columns, and the cell values are the correlations between the variables.

## 27. What is a scatter plot?

The standard way to visualize the relationship that two measured data variables have.

It is a plot in which the x-axis is the value of one variable, and the y-axis the value of another.

# Sampling Distributions

## 28. Sample vs population

A sample is a subset of data from a larger dataset (a population).

A population in statistics is a large, defined but sometimes theoretical or imaginary, set of data.

## 29. What is random sampling, stratified and simple random sampling?

Random Sampling: Drawing elements into a sample at random.

Stratified Sampling: Dividing the population into strata and randomly sampling from each strata.

Simple Random Sampling: The sample that results from random sampling without stratifying the population.

## 30. What is bias?

Statistical bias refers to measurement or sampling errors that are systematic and produced by the measurement or sampling process.

An important distinction should be made between errors due to random chance, and errors due to bias.

## 31. How to ensure against bias?

Random sampling can reduce bias and facilitate quality improvement.

Time and effort spent on random sampling not only reduce bias, but also allow greater attention to data quality.

Specifying a hypothesis, then collecting data following randomization and random sampling principles ensures against bias.

## 32. What is regression to the mean?

Regression to the mean refers to a phenomenon involving successive measurements on a given variable: extreme observations tend to be followed be more central ones.

Attaching special focus and meaning to the extreme value can lead to a form of selection bias.

## 33. Explain sampling distribution

The distribution of some sample statistic, over many samples drawn from the same population.

The distribution of a sample statistic such as the mean is likely to be more regular and bell-shaped than the distribution of the data themselves.

The larger the sample that the statistic is based on, the more this is true.

## 34. Define the central limit theorem.

The means drawn from multiple samples will be shaped like the familiar bell-shaped normal curve, even if the source population is not normally distributed.

Provided that:

- the sample size is large enough
- the departure of the data from normality is not too great

## 35. Standard error

A metric that sums up the variability in the sampling distribution for a statistic.

The standard error can be estimated using a statistic based on the standard deviation of the sample values, and the sample size.

## 36. What is a bootstrap sample?

A sample taken with replacement from an observed dataset.

## 37. What is resampling?

The process of taking repeated samples from observed data. It includes both bootstraping and shuffling procedures.

In practice, it is not necessary to actually replicate the sample a huge number of times. We simply replace each observation after each draw - we sample with replacement.

## 38. Resampling vs bootstrapping

Resampling also includes shuffling procedures, where multiple samples are combined, and the sampling may be done without replacement.

Bootstraping always implies sampling with replacement.

## 39. Confidence levels

The percentage of confidence intervals, constructed in the same way from the same population, expected to contain the statistic of interest.

For a data scientist, a confidence interval is a tool to get an idea of how variable a sample result might be.

## 40. How can you construct a confidence interval?

Through bootstraping.

## 41. Normal distributions.

In a normal distribution 68% of the data lie within one standard deviation of the mean, and 95% lie within two standard deviations.

## 42. Why is normal distribution important?

The utility of the normal distribution derives from the fact that many statistics are normally distributed in their sampling distribution.

## 43. What is a QQ plot?

A QQ-Plot is used to visually determine how close a sample is to the normal distribution.

The QQ-Plot orders the z-scores from low to high, and plots each value’s z-score on the y-axis. The x-axis is the corresponding quantile of a normal distribution for that value’s rank.

## 44. Long tailed distributions.

While the normal distribution is often appropriate and useful with respect to the distribution of errors and sample statistics, it typically does not characterize the distribution of actual raw data.

Most times, the distribution is skewed (asymmetric), having a tail, a long narrow portion where relatively extreme values occur at low frequency.

## 45. Black swans

Assuming a normal distribution can lead to the underestimation of extreme events, a dangerous long tailed distribution.

## 46. Binomial distributions.

The binomial distribution is the distribution of outcomes in a specific number of binomial trials.

## 47. What is a binomial trial?

A binomial trial is an experiment with two possible outcomes, one with probability p and the other with probabiltiy 1-p.

With a large number of trial, and provided p is not too close to 0 or 1, the binomial distribution can be approximated by the normal distribution.

## 48. Poisson distributions.

The frequency distribution of the number of events in sampled units of time or space.

The key parameter in a Poisson distribution is lambda. This is the mean number of events that occurs in a specified interval of time or space.

## 49. Exponential distributions.

The frequency distribution of the time or distance from one event to the next event.

## 50. Weibull distributions.

A generalized version of the exponential, in which the event rate is allowed to shift over time.

## 51. What is statistical inference?

The process of using data analysis to deduce properties of an underlying probability distribution.

It is assumed that the observed dataset is sampled from a larger population.

## 52. How to determine error in sample estimates?

Through sampling with replacement from an observed dataset.

# Statistics & Significance Testing

## 53. What is the role of a statistical experiment?

The goal is to design an experiment in order to confirm or reject a hypothesis.

## 54. Define a statistical inference pipeline.

Formulate a hypothesis -> Design Experiment -> Collect data -> Draw conclusions.

## 55. Define A/B testing.

An A/B test is an experiment with two groups to establish which of two treatments is superior.

## 56. Define treatments and treatment groups.

A treatment is a procedure to which a group is exposed.

A treatment group is a group of subjects exposed to a treatment.
A control group is a group of subjects not exposed to a treatment.

## 57. What are significance tests?

Tests that have the purpose to clarify whether random chance is responsible for an effect.

## 58. What is the null hypothesis?

The hypothesis that random chance is responsible for an effect.

## 59. Explain the hypothesis test.

The hypothesis test assumes that the null hypothesis is true, creates a null model and tests whether the effect observed is a reasonable outcome of that model.

## 60. What is a permutation test?

The procedure of combining two or more samples and randomly reallocating the observations to resamples.

## 61. What are the steps in a permutation test?

1. Combine the results from different groups in a single dataset.
2. Shuffle the combined data then randomly draw (without replacement) a resample of the same size as group A.
3. From the remaining data, ramdomly draw (without replacement) a resample of the same size as group B.
4. Whatever statistic was calculated on the original samples, calculate for for the resamples.

## 62. What is a bootstrap permutation test?

In a bootstrap permutation test the draws from the combined data are made with replacement.

## 63. What is statistical significance?

The metric that measures whether an experiment yields a result more extreme than what random chance might produce.

## 64. What is the p-value?

Given a model that assumes the null hypothesis, the p-value is the probability of obtaining results as extreme as the observed results.

## 65. What is Alpha?

The probability threshold that chance results must surpass in order for the actual outcomes to be considered statistically significant.

## 66. Type 1 errors.

Mistakenly concluding an effect is real, when it's due to chance.

## 67. Type 2 errors.

Mistakenly concluding and effect is due to chance when it's real.

## 68. Problems with p-value.

A significant p-value does not provide a good measure of evidence regarding a model or hypothesis.

P-values don't measure the probability that the hypothesis is true, they indicate how incompatible is the data with a specified statistical model, in the usual case, the null model.

## 69. What is ANOVA (ANalysis Of VAriance)?

A statistical procedure that tests for a significant difference among multiple groups of numerical data.

## 70. What is the F-statistic?

The F-statistic is statistical test for ANOVA, based on the ratio of the variance across group means to the variance due to residual errors.
The higher this ratio, the more statistically significant the result.
